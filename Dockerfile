FROM docker.io/library/node:14.15.5-buster as builder
WORKDIR /backend/
ADD ./package.json /backend/
ADD ./package-lock.json /backend/
ADD ./tsconfig.json /backend/
ADD ./src /backend/src
RUN npm install --production
RUN npm run build

FROM docker.io/library/node:14.15.5-alpine3.13
WORKDIR /backend/
COPY --from=builder /backend /backend/

CMD npm -s start
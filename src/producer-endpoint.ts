"use strict";

import restify from "restify";
import { goodRes } from "./app-service";
import ProducerService from "./producer-service";

export default {
    onSend
}

function onSend(req: restify.Request, res: restify.Response, next: restify.Next) {
    return Promise.resolve().then(() => {
        const params = req.body;

        return ProducerService.Instance.send(
            params?.to,
            params?.cc,
            params?.bcc,
            params?.code,
            params?.tags,
            params?.files
        ).then(result => {
            return goodRes(res, result);
        });
    });
}

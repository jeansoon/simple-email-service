"use strict";

import restify from "restify";
import restifyError from "restify-errors";
import corsMiddleware from "restify-cors-middleware";
import ElapsedTime from "elapsed-time";

import { allowNull, allowNullOrEmpty, checkFormat, checkNumber, checkString, isUndefinedOrNull, isUndefinedOrNullOrEmpty, notAllowNullOrEmpty } from "mxw-libs-utils";
import { clog, levels } from "mxw-libs-clogger";
import { describeError, errors, throwError } from "mxw-libs-errors";

import EmailService from "./email/email-service";
import IngressService from "./ingress-service";
import ProducerService from "./producer-service";

import ProducerEndpoint from "./producer-endpoint";

const { name, version } = require("../package.json");

enum ContentType {
    JSON = "JSON",
    RAW = "RAW"
}

export default class AppService {
    private static self: AppService;

    private server: restify.Server;

    private auths: {
        [path: string]: {
            authCheck: (token: string) => any,
            logExcluded: boolean,
            preCheckCallback: any
        }
    } = {};

    public config: {
        KAFKA_URL: string;
        KAFKA_GROUP?: string;
        KAFKA_TOPIC: string;
        EMAIL_PROVIDER_URL?: string;
        EMAIL_FROM?: string;
        EMAIL_CONCURRENT?: number;
    };

    /**
     * Create a singleton
     */
    public static get Instance(): AppService {
        return this.self || (this.self = new this());
    }

    /**
     * Serve the health probe
     * 
     * @param req Http request
     * @param res Http response
     * @param next The next route
     */
    private onLiveness(req: restify.Request, res: restify.Response, next: restify.Next) {
        return Promise.resolve().then(() => {
            res.json(200);
        });
    }

    /**
     * Serve the readiness probe, let the load balancer control the traffic load
     *  
     * @param req Http request
     * @param res Http response
     * @param next The next route
     */
    private onReadiness(req: restify.Request, res: restify.Response, next: restify.Next) {
        return Promise.resolve().then(() => {
            if (IngressService.isReadiness) {
                res.json(200);
            }
            else {
                res.json(429);
            }
        });
    }

    /**
     * Serve the root probe for testing
     *  
     * @param req Http request
     * @param res Http response
     * @param next The next route
     */
    public onRoot(req: restify.Request, res: restify.Response, next: restify.Next) {
        return Promise.resolve().then(() => {
            res.json({
                message: "Welcome to the coolest API on earth - " + this.server.name + ":" + version + "!"
            });
        });
    }

    /**
     * Initialize the service
     */
    public init() {
        return Promise.resolve().then(() => {
            try {
                this.config = checkFormat({
                    KAFKA_URL: notAllowNullOrEmpty(checkString),
                    KAFKA_GROUP: allowNullOrEmpty(checkString),
                    KAFKA_TOPIC: notAllowNullOrEmpty(checkString),
                    EMAIL_PROVIDER_URL: notAllowNullOrEmpty(checkString),
                    EMAIL_FROM: allowNullOrEmpty(checkString),
                    EMAIL_CONCURRENT: allowNull(checkNumber, 1)
                }, {
                    KAFKA_URL: process.env.KAFKA_URL,
                    KAFKA_GROUP: process.env.KAFKA_GROUP,
                    KAFKA_TOPIC: process.env.KAFKA_TOPIC,
                    EMAIL_PROVIDER_URL: process.env.EMAIL_PROVIDER_URL,
                    EMAIL_FROM: process.env.EMAIL_FROM,
                    EMAIL_CONCURRENT: process.env.EMAIL_CONCURRENT
                });
            }
            catch (error) {
                const { message } = describeError(error);
                throwError(errors.NOT_CONFIGURED, "Environment: " + message, { error });
            }

            // Initialize server
            this.server = restify.createServer({
                name,
                version,
                socketio: true
            });

            const origins: string[] = (!isUndefinedOrNullOrEmpty(process.env.CORS)) ? process.env.CORS.split(",") : ["*"];

            const cors = corsMiddleware({
                origins: origins,
                allowHeaders: [
                    "Origin", "X-Requested-With", "Content-Type", "Accept", "Lang", "Authorization", "Accept-Version",
                    "Content-Language", "app-id", "Location", "Machine-Id"
                ],
                exposeHeaders: ["GET", "POST", "PATCH", "HEAD", "OPTIONS", "X-Requested-With", "X-Metadata"]
            });

            this.server.pre(cors.preflight);
            this.server.use(cors.actual);
            this.server.use(restify.plugins.queryParser());
            this.server.use(restify.plugins.authorizationParser());

            this.server.use(restify.plugins.bodyParser({
                mapParams: true,
                mapFiles: false,
                keepExtensions: true,
                multiples: true,
                maxBodySize: 1 * 1024 * 1024,
            }));

            const self = this;

            this.server.pre(restify.pre.dedupeSlashes());
            this.server.pre(function (req: restify.Request, res: restify.Response, next: restify.Next) {
                req["et"] = ElapsedTime.new().start();

                let auth = self.auths[req.method + ":" + req.path().toLowerCase()];

                if (!auth) {
                    auth = self.auths[req.method + ":*"];
                }

                if (auth) {
                    if (!auth.logExcluded) {
                        const ip = getIp(req);
                        clog(levels.NORMAL, `REQ: ${req.method} ${req.url} HTTP/${req.httpVersion} @${ip}`);
                    }
                }

                return next();
            });

            this.server.use(function (req: restify.Request, res: restify.Response, next: restify.Next) {
                // Authentication
                // TECHNICAL DEBT: NOT IMPLEMENTED DUE TO TIME CONSTRAINT
                return next();
            });
            this.server.on("after", function (req, res, route, err) {
                const ip = getIp(req);

                try {
                    const contentLength = res.getHeaders()["content-length"];

                    if ((200 <= res.statusCode && 299 >= res.statusCode) || (300 <= res.statusCode && 399 >= res.statusCode)) {
                        if (isUndefinedOrNullOrEmpty(res.ret) || "0" == res.ret) {
                            let auth = self.auths[req.method + ":" + req.path().toLowerCase()];

                            if (!auth) {
                                auth = self.auths[req.method + ":*"];
                            }

                            if (auth && !auth.logExcluded) {
                                clog(levels.WARNING, `RES: ${req.method} ${req.url} HTTP/${req.httpVersion} @${ip}`,
                                    `OK ${contentLength ? contentLength : "-"}, elapsed: ${req.et.getValue()}`);
                            }
                        }
                        else {
                            clog(levels.ERROR, `RES: ${req.method} ${req.url} HTTP/${req.httpVersion} @${ip}`,
                                `${res.ret} ${contentLength ? contentLength : "-"}, elapsed: ${req.et.getValue()} ERROR: ${err ? err.toString(true) : "Unknown error"}`);
                        }
                    }
                    else {
                        clog(levels.ERROR, `RES: ${req.method} ${req.url} HTTP/${req.httpVersion} @${ip}`,
                            `${res.statusCode} ${contentLength ? contentLength : "-"}, elapsed: ${req.et.getValue()} ERROR: ${err ? err.toString(true) : "Unknown error"}`);
                    }
                }
                catch (error) {
                    clog(levels.ERROR, `RES: ${req.method} ${req.url} HTTP/${req.httpVersion} @${ip}`,
                        `${res.statusCode} -, elapsed: ${req.et.getValue()} Error: ${error}`);
                }
            });

            this.server.on("restifyError", function (req, res, err, callback) {
                let code: string;

                err.toJSON = () => {
                    return {
                        ret: code,
                        msg: (undefined != err && err.message) ? err.message : "[EMPTY]"
                    };
                };

                err.toString = (details?: boolean) => {
                    let message;
                    if (details && undefined != err && err.context && err.context.reason) {
                        message = err.context.reason;
                    }
                    if (!message) {
                        message = code + ": " + ((undefined != err && err.message) ? err.message : "[EMPTY]");
                    }
                    return message;
                };

                if (undefined != err && err.context && err.context.code) {
                    code = err.context.code;
                }
                else {
                    code = (undefined != err && err.body && err.body.code) ? err.body.code : "-1";
                }

                res["ret"] = code;

                return callback();
            });

            return this;
        });
    }

    /**
     * Activate the service
     */
    public run() {
        return Promise.resolve().then(() => {
            return EmailService.init(this.config.EMAIL_PROVIDER_URL, this.config.EMAIL_FROM, this.config.EMAIL_CONCURRENT);
        }).then(() => {
            return IngressService.init(this.config.KAFKA_URL, this.config.KAFKA_GROUP, this.config.KAFKA_TOPIC);
        }).then(() => {
            return ProducerService.init(this.config.KAFKA_URL, this.config.KAFKA_TOPIC);
        }).then(() => {
            const authByPass = undefined;

            /* HEALTH ENDPOINTS */
            this.entryPoint(this.server, "/", this.onRoot, undefined, ["GET", "POST"], authByPass, ContentType.JSON, true);
            this.entryPoint(this.server, "/liveness", this.onLiveness, undefined, ["GET", "POST"], authByPass, ContentType.RAW, true);
            this.entryPoint(this.server, "/readiness", this.onReadiness, undefined, ["GET", "POST"], authByPass, ContentType.RAW, true);

            /* SIMULATION */
            this.entryPoint(this.server, "/send", ProducerEndpoint.onSend, undefined, ["POST"], authByPass, ContentType.JSON, false);

            return new Promise<string>((resolve, reject) => {
                try {
                    this.server.listen(80, () => {
                        return resolve(this.server.url);
                    });
                }
                catch (error) {
                    return reject(error);
                }
            });
        });
    }

    public term() {
        return Promise.resolve().then(() => {
            if (this.server) {
                return new Promise<void>((resolve, reject) => {
                    try {
                        this.server.close(() => {
                            return resolve();
                        });
                    }
                    catch (error) {
                        return reject(error);
                    }
                });
            }
        }).then(() => {
            return EmailService.term();
        });
    }

    private entryPoint(server: restify.Server, uri: string, callback: any, preCheckCallback: any, methods: string[], authCheck: (token: string) => any, contentType: ContentType, logExcluded: boolean) {
        let self = this;

        for (let method of methods) {
            this.auths[method + ":" + uri.toLowerCase()] = {
                authCheck,
                logExcluded,
                preCheckCallback
            };
        }

        if (0 <= methods.indexOf("GET")) server.get(uri, function (req, res, next) { self.onRequest(callback, contentType, req, res, next) });
        if (0 <= methods.indexOf("POST")) server.post(uri, function (req, res, next) { self.onRequest(callback, contentType, req, res, next) });
        if (0 <= methods.indexOf("PATCH")) server.patch(uri, function (req, res, next) { self.onRequest(callback, contentType, req, res, next) });
        if (0 <= methods.indexOf("PUT")) server.put(uri, function (req, res, next) { self.onRequest(callback, contentType, req, res, next) });
        if (0 <= methods.indexOf("DELETE")) server.del(uri, function (req, res, next) { self.onRequest(callback, contentType, req, res, next) });

        if (0 <= methods.indexOf("HEAD")) server.head(uri, function (req, res, next) { self.onRequest(callback, contentType, req, res, next) });
        if (0 <= methods.indexOf("OPTIONS")) server.opts(uri, function (req, res, next) { self.onRequest(undefined, contentType, req, res, next) });
    }

    /**
     * The http request entry point
     * 
     * @param func The process function callback
     * @param req Http request
     * @param res Http response
     * @param next The next route
     */
    private onRequest(func, contentType: ContentType, req: restify.Request, res: restify.Response, next: restify.Next) {
        switch (req.method) {
            case "GET":
            case "HEAD":
                break;

            case "POST":
            case "PUT":
            case "PATCH":
            case "DELETE":
                switch (contentType) {
                    case ContentType.JSON:
                        if (!req.is("json")) {
                            return next(new restifyError.BadRequestError({ info: { code: "ContentTypeNotSupported" } },
                                "Content type is not supported"));
                        }
                }
                break;

            case "OPTIONS":
                res.json(200);
                return next();
        }

        return func.call(this, req, res, next).then((nextContext) => {
            if (!isUndefinedOrNullOrEmpty(nextContext)) {
                throw nextContext;
            }

            return next();
        }).catch(error => {
            let { code, message, reason } = errors.describeError(error);

            clog(levels.INFO, "Error", code, ":", reason);

            if (errors.DATABASE_ERROR == code) {
                code = isUndefinedOrNullOrEmpty(error.ret) ? "SYSTEM_BUSY" : error.ret;
                message = isUndefinedOrNullOrEmpty(error.msg) || "-1" === code || "999" === code || "9999" === code ? "System Busy" : error.msg;
            }

            return this.translateError(req.headers["content-language"], code, message).then((translated) => {
                errors.throwError(translated.code, translated.message, {});
            }).catch(error => {
                const { code, message } = errors.describeError(error);
                const e = new restifyError.InternalServerError({
                    info: {
                        code: isUndefinedOrNullOrEmpty(code) ? "InternalServerError" : code,
                        reason
                    }
                }, isUndefinedOrNullOrEmpty(message) ? "Something went wrong" : message);
                return next(e);
            });
        });
    }

    private translateError(language: string, code: string, message: string) {
        return Promise.resolve().then(() => {
            if (!language) {
                language = "en_US"; // Default language
            }

            // Translate error message
            // TECHNICAL DEBT: NOT IMPLEMENTED DUE TO TIME CONSTRAINT

            return { code, message };
        });
    }
}

export function getIp(req: restify.Request) {
    const ip =
        req.headers?.['x-original-forwarded-for'] as string ||
        req.headers?.['x-forwarded-for'] as string ||
        req.headers?.['x-real-ip'] as string ||
        "0.0.0.0";
    return ip;
}

export function goodRes(res: restify.Response, data?: any) {
    const resp = {
        ret: "0"
    }
    if (!isUndefinedOrNull(data)) {
        delete data.ret;
        resp["data"] = data;
    }
    res.json(resp);
}

export function badRes(res: restify.Response, ret: string, msg?: string, data?: any) {
    const resp = {
        ret, msg
    }
    if (!isUndefinedOrNull(data)) {
        delete data.ret;
        resp["data"] = data;
    }
    res.json(resp);
}

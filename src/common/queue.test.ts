'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import Sinon from "sinon";

import Queue from "./queue";

let sandbox: Sinon.SinonSandbox;

describe('Suite: Queue', function () {
    beforeEach(function () {
        sandbox = Sinon.createSandbox();
    });

    afterEach(function () {
        if (sandbox) {
            sandbox.restore();
            sandbox = undefined;
        }
    });

    it("Concurrency limit", function () {
        let max = 0;
        let count = 0;
        const concurrent = 200;
        const simpleQueue = new Queue<void>(concurrent);

        const simpleTask = () => {
            return new Promise<void>((resolve) => {
                if (++count > max){
                    max = count;
                }

                setTimeout(() => {
                    count--;
                    return resolve();
                }, 100);
            });
        };

        let promises = [];

        for (let i = 0; 1000 > i; i++) {
            promises.push(simpleQueue.enqueue(simpleTask));
        }

        return Promise.all(promises).then(()=>{
            expect(max).is.lessThanOrEqual(concurrent);
            expect(max).is.greaterThan(0);
        });
    });
});

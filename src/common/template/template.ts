"use strict";

import BaseTemplate, { TAGS } from "./template-base";

export default class TemplateService {
    private static self: TemplateService;

    private list: {
        [orgId: string]: {
            [code: string]: Template
        }
    };

    public static get Instance(): TemplateService {
        return this.self || (this.self = new TemplateService());
    }

    public static get isEnabled() {
        return this.self ? true : false;
    }

    public static init() {
        return Promise.resolve().then(() => {
            const self = new this();

            // // Trigger to load template
            return self.getTemplate(0, "").then(() => {
                this.self = self;
                return self;
            });
        });
    }

    public static term() {
        return Promise.resolve().then(() => {
            this.self = undefined;
        });
    }

    public getTemplate(orgId: number, code: string): Promise<Template> {
        return Promise.resolve().then(() => {
            if (!this.list) {
                // Load template from persistence storage
                // TECHNICAL DEBT: NOT IMPLEMENTED DUE TO TIME CONSTRAINT
                this.list = {};
            }
        }).then(() => {
            const templates = this.list[orgId] || [];

            return templates?.[code];
        });
    }
}

export class Template extends BaseTemplate {
    private sp: string;

    constructor(name: string, code: string, sp: string, content: string, ref?: any) {
        super(name, code, content, ref);
        this.sp = sp;
    }

    public render(tags?: TAGS, params?: any[], overrides?: any) {
        return Promise.resolve().then(() => {
            let input = tags || {};

            if (this.sp) {
                // Query extra info from database or external services
                // TECHNICAL DEBT: NOT IMPLEMENTED DUE TO TIME CONSTRAINT
            }

            return input;
        }).then(tags => {
            return {
                tags,
                data: super.parse(tags)
            };
        });
    }
}

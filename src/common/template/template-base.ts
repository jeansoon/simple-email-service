"use strict";

import { base64, toUtf8String } from "mxw-libs-utils";

export type TAGS = { [key: string]: string | number };

export default abstract class BaseTemplate {
    private _name: string;
    private _code: string;
    private _content: string;
    private _ref: any;

    constructor(name: string, code: string, content: string, ref?: any) {
        this._name = name;
        this._code = code;
        this._ref = ref;

        try {
            this._content = toUtf8String(base64.decode(content));
        }
        catch (error) {
            this._content = content;
        }
    }

    public get name() {
        return this._name;
    }
    public get code() {
        return this._code;
    }
    public get content() {
        return this._content;
    }
    public get ref() {
        return this._ref;
    }

    protected parse(tags?: TAGS) {
        const getTag = function (script: string, index: number) {
            let start = script.indexOf("{{", index);

            if (0 <= start) {
                start += 2;
                const end = script.indexOf("}}", start);

                if (start <= end) {
                    return {
                        tag: script.substring(start, end),
                        start: start - 2,
                        end: end + 2
                    };
                }
            }
            return {
                tag: null,
                start: script.length,
                end: script.length
            };
        }

        const content = this.content;
        let output = "";

        if (this.content) {
            let index = 0;

            while (content.length > index) {
                const { tag, start, end } = getTag(content, index);

                if (start > index) {
                    output += content.substring(index, start);
                }
                if (tag) {
                    const value = tags?.[tag] ? tags?.[tag] : "";
                    output += value;
                }
                index = end;
            }
        }
        return output;
    }
}

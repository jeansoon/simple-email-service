'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import Sinon from "sinon";
import Kafka, { ConsumerRunConfig } from "kafkajs";
import { KafkaConsumer, KafkaProducer, EachMessagePayload, CONNECTION_STATUS, TopicPartitionOffsetAndMetadata } from "./kafka";
import { ProducerRecord } from 'kafkajs';

let sandbox: Sinon.SinonSandbox;

describe('Suite: Kafka', function () {
    beforeEach(function () {
        sandbox = Sinon.createSandbox();
    });

    afterEach(function () {
        if (sandbox) {
            sandbox.restore();
            sandbox = undefined;
        }
    });

    it("Initialize consumer", function () {
        const kafkaUrl = "hello.awesome:9092";
        const kafkaGroup = "AWESOME";
        const kafkaTopic = "AWESOME-EMAIL";
        const kafkaPartition = 0;

        const statusCallbacks: {
            [eventName: string]: () => void;
        } = {};

        let fakeProducer: (payload: EachMessagePayload) => void;

        const fakeConsumer = {
            consumer: function () {
                return {
                    on: function (eventName: string, cb: () => void) {
                        expect(eventName).is.a("string");
                        expect(cb).is.a("function");
                        statusCallbacks[eventName] = cb;
                    },
                    connect: function () {
                        statusCallbacks["consumer.connect"]?.();
                        return Promise.resolve();
                    },
                    subscribe: function (topic: string, fromBeginning: boolean) {
                        return Promise.resolve();
                    },
                    run: function (config: ConsumerRunConfig) {
                        expect(config).is.an("object");
                        expect(config.eachMessage).is.a("function");

                        fakeProducer = config.eachMessage;
                        return Promise.resolve();
                    },
                    disconnect: function () {
                        statusCallbacks["consumer.disconnect"]?.();
                        return Promise.resolve();
                    },
                    commitOffsets: function (topicPartitions: Array<TopicPartitionOffsetAndMetadata>) {
                        expect(topicPartitions).is.an("array").lengthOf.greaterThan(0);
                        expect(topicPartitions[0].offset).is.a("string").eq("1");
                        return Promise.resolve();
                    }
                }
            },
        };
        sandbox.stub(Kafka, "Kafka").returns(fakeConsumer);

        let received = false;

        return KafkaConsumer.init(kafkaUrl, kafkaGroup, kafkaTopic, kafkaPartition, (payload) => {
            expect(payload).is.an("object");
            expect(payload).has.property("topic").is.a("string").eq(kafkaTopic);
            expect(payload).has.property("partition").is.a("number").eq(kafkaPartition);
            expect(payload).has.property("message").is.an("object");
            expect(payload.message).has.property("offset").is.a("string").eq("0");
            expect(payload.message).has.property("value").to.be.instanceof(Buffer);
            expect(payload.message.value.toString()).is.a("string").eq("Hello Consumer");

            received = true;
            return Promise.resolve();
        }).then(instance => {
            expect(instance.status).eq(CONNECTION_STATUS.READY);

            fakeProducer({
                topic: kafkaTopic,
                partition: kafkaPartition,
                message: {
                    key: undefined,
                    value: Buffer.from("Hello Consumer"),
                    timestamp: "1635912932995",
                    size: 0,
                    attributes: 0,
                    offset: "0"
                }
            });
            expect(received).is.true;

            return instance.term();
        });
    });

    it("Initialize producer", function () {
        const kafkaUrl = "hello.awesome:9092";
        const kafkaTopic = "AWESOME-EMAIL";
        const kafkaPartition = 0;

        const statusCallbacks: {
            [eventName: string]: () => void;
        } = {};

        let sent = false;

        const fakeProducer = {
            producer: function () {
                return {
                    on: function (eventName: string, cb: () => void) {
                        expect(eventName).is.a("string");
                        expect(cb).is.a("function");
                        statusCallbacks[eventName] = cb;
                    },
                    connect: function () {
                        statusCallbacks["producer.connect"]?.();
                        return Promise.resolve();
                    },
                    disconnect: function () {
                        statusCallbacks["producer.disconnect"]?.();
                        return Promise.resolve();
                    },
                    send: function (payload: ProducerRecord) {
                        expect(payload).is.an("object");
                        expect(payload).has.property("topic").is.a("string").eq(kafkaTopic);
                        expect(payload).has.property("messages").is.an("array").lengthOf(1);
                        expect(payload.messages[0]).has.property("partition").is.a("number").eq(kafkaPartition);
                        expect(payload.messages[0]).has.property("value").is.a("string").eq("Hello Producer");

                        sent = true;
                        return Promise.resolve();
                    }
                }
            },
        };
        sandbox.stub(Kafka, "Kafka").returns(fakeProducer);

        return KafkaProducer.init(kafkaUrl).then(instance => {
            expect(instance.status).eq(CONNECTION_STATUS.READY);

            return instance.send(kafkaTopic, "Hello Producer", kafkaPartition).then(() => {
                expect(sent).is.true;
                return instance.term();
            });
        });
    });
});

"use strict";

export default class Queue<T> {
    private tasks: {
        handler: () => Promise<T>;
        callback: (result: T) => void;
        error: (error) => void;
    }[] = [];

    private isAlive = 0;
    private concurrent = 1;

    constructor(concurrent?: number) {
        if (!(0 < concurrent)) {
            concurrent = 1;
        }
        this.concurrent = concurrent;
    }

    public enqueue(handler: () => Promise<T>) {
        return new Promise<T>((resolve, reject) => {
            const task = {
                handler,
                callback: (result) => {
                    return resolve(result);
                },
                error: (error) => {
                    return reject(error);
                }
            };

            this.tasks.push(task);
            this.scheduler();
        });
    }

    private scheduler() {
        if (this.isAlive < this.concurrent) {
            this.isAlive++;

            setTimeout(() => {
                if (0 < this.tasks.length) {
                    const task = this.tasks.shift();

                    return task.handler().then(result => {
                        task.callback(result);
                        
                        this.isAlive--;
                        return this.scheduler();
                    }).catch(error => {
                        task.error(error);

                        this.isAlive--;
                        return this.scheduler();
                    });
                }

                this.isAlive--;
            }, 0);
        }
    }
}

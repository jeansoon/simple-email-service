"use strict";

import { Kafka, EachMessagePayload, ProducerRecord, Consumer, ConsumerRunConfig, Producer, ProducerConfig, logLevel, TopicPartitionOffsetAndMetadata } from "kafkajs";
import { errors, throwError } from "mxw-libs-errors";
import { checkFormat, checkString, notAllowNullOrEmpty, checkNumber, isUndefinedOrNullOrEmpty, checkSafeBigNumber } from "mxw-libs-utils";

export { EachMessagePayload, ProducerRecord, logLevel, TopicPartitionOffsetAndMetadata };

export type StatusCallback = (this: KafkaConsumer, status: CONNECTION_STATUS) => void;
export type ConsumerHandler = (this: KafkaConsumer, payload: EachMessagePayload) => Promise<any>;

export enum CONNECTION_STATUS {
    DISCONNECTED = "DISCONNECTED",
    READY = "READY",
    CONNECT = "CONNECT",
    RECONNECT = "RECONNECT",
};

export class KafkaConnection {
    protected client: Kafka;
    protected statusCallback: StatusCallback;
    protected _status: CONNECTION_STATUS;

    constructor(url: string, overrides?: any, statusCallback?: StatusCallback) {
        const params: {
            url: string;
        } = checkFormat({
            url: notAllowNullOrEmpty(checkString),
        }, { url });

        this.client = new Kafka({ brokers: [params.url], ...overrides });

        this.statusCallback = statusCallback;
    }

    public term() {
        return Promise.resolve().then(() => {
            this.client = undefined;
        });
    }

    protected get status() {
        return this._status;
    }

    protected updateStatus(status: CONNECTION_STATUS) {
        if (this._status !== status) {
            this._status = status;

            if (this.statusCallback) {
                this.statusCallback.apply(this, [this._status]);
            }
        }
    }
}

export class KafkaConsumer extends KafkaConnection {
    private consumer: Consumer;

    constructor(url: string, overrides?: any, statusCallback?: StatusCallback) {
        super(url, overrides, statusCallback);
    }

    static init(url: string, groupId: string, topic: string, partition: number = 0, handler: ConsumerHandler, overrides?: any, statusCallback?: StatusCallback) {
        return Promise.resolve().then(() => {
            const params: {
                groupId: string;
                topic: string;
                partition: number;
                handler: ConsumerHandler;
            } = checkFormat({
                groupId: notAllowNullOrEmpty(checkString),
                topic: notAllowNullOrEmpty(checkString),
                partition: checkNumber,
                handler: function (value) {
                    if ("function" !== typeof value) {
                        throwError(errors.INVALID_FORMAT, "Invalid consumer handler", { handler: value });
                    }
                    return value;
                }
            }, {
                groupId,
                topic,
                partition,
                handler
            });

            const self = new KafkaConsumer(url, overrides, statusCallback);

            const consumer = self.consumer = self.client.consumer({
                groupId: params.groupId,
                ...overrides
            });

            const runConfig: ConsumerRunConfig = {
                eachMessage: function (payload: EachMessagePayload) {
                    const offset = checkSafeBigNumber(payload?.message?.offset);

                    return params.handler.apply(self, [payload]).then(() => {
                        // The commit offset need to plus 1
                        return consumer.commitOffsets([
                            {
                                topic: payload?.topic,
                                partition: payload?.partition,
                                offset: offset.plus(1).toString()
                            }
                        ]);
                    });
                },
                autoCommit: false,
                ...overrides
            }

            consumer.on("consumer.connect", () => { self.updateStatus(CONNECTION_STATUS.READY); });
            consumer.on("consumer.disconnect", () => { self.updateStatus(CONNECTION_STATUS.DISCONNECTED); });
            consumer.on("consumer.stop", () => { self.updateStatus(CONNECTION_STATUS.DISCONNECTED); });
            consumer.on("consumer.crash", () => { self.updateStatus(CONNECTION_STATUS.DISCONNECTED); });

            return consumer.connect().then(() => {
                const fromBeginning = !isUndefinedOrNullOrEmpty(overrides?.fromBeginning) ? overrides.fromBeginning : true;

                return consumer.subscribe({
                    topic: params.topic,
                    fromBeginning
                });
            }).then(() => {
                return consumer.run(runConfig);
            }).then(() => {
                return self;
            });
        });
    }

    public term() {
        return Promise.resolve().then(() => {
            if (this.consumer) {
                return this.consumer.disconnect();
            }
        }).then(() => {
            this.consumer = undefined;
            return super.term();
        });
    }

    public get status() {
        return super.status;
    }
}

export class KafkaProducer extends KafkaConnection {
    private producer: Producer;

    constructor(url: string, overrides?: any, statusCallback?: StatusCallback) {
        super(url, overrides, statusCallback);
    }

    static init(url: string, overrides?: any, statusCallback?: StatusCallback) {
        return Promise.resolve().then(() => {
            const self = new KafkaProducer(url, overrides, statusCallback);

            const config: ProducerConfig = {
                ...overrides
            };

            const producer = self.producer = self.client.producer(config);

            producer.on("producer.connect", () => { self.updateStatus(CONNECTION_STATUS.READY); });
            producer.on("producer.disconnect", () => { self.updateStatus(CONNECTION_STATUS.DISCONNECTED); });

            return producer.connect().then(() => {
                return self;
            });
        });
    }

    public term() {
        return Promise.resolve().then(() => {
            if (this.producer) {
                return this.producer.disconnect();
            }
        }).then(() => {
            this.producer = undefined;
            return super.term();
        });
    }

    public get status() {
        return super.status;
    }

    public send(topic: string, value: string, partition: number = 0, key?: string, overrides?: any) {
        return Promise.resolve().then(() => {
            return this.producer.send({
                topic,
                messages: [{
                    key,
                    value,
                    partition,
                    ...overrides
                }]
            });
        });
    }
}

"use strict";

import { clog, levels } from "mxw-libs-clogger";
import { errors, throwError } from "mxw-libs-errors";
import { allowNullOrEmpty, arrayOf, checkFormat, checkNumber, checkString, notAllowNullOrEmpty } from "mxw-libs-utils";
import { TAGS } from "./common/template/template-base";
import { CONNECTION_STATUS, KafkaConsumer, EachMessagePayload, StatusCallback, logLevel } from "./common/kafka";
import EmailService from "./email/email-service";

export default class IngressService {
    private static self: IngressService;

    private consumer: KafkaConsumer;

    static get Instance() {
        return this.self || throwError(errors.NOT_INITIALIZED, "IngressService initialization is required");
    }

    public static get isEnabled() {
        return this.self ? true : false;
    }

    public static get isReadiness() {
        if (this.self && this.self.consumer) {
            return CONNECTION_STATUS.READY === this.self.consumer.status;
        }
        return false;
    }

    static init(url: string, groupId: string, topic: string, partition: number = 0) {
        return Promise.resolve().then(() => {
            const self = new this();

            const statusCallback: StatusCallback = (status) => {
                clog(levels.NORMAL, `Service event queue status is ${status}: ${topic}`);
            };

            clog(levels.NORMAL, `Service event queue status is CONNECTING: ${topic}`);

            const overrides = {
                logLevel: logLevel.ERROR
            };

            return KafkaConsumer.init(url, groupId, topic, partition, self.process, overrides, statusCallback).then(consumer => {
                self.consumer = consumer;
                this.self = self;
            });
        });
    }

    static term() {
        return Promise.resolve().then(() => {
            if (this.self) {
                if (this.self.consumer) {
                    return this.self.consumer.term();
                }
            }
        }).then(() => {
            this.self = undefined;
        });
    }

    private process(payload: EachMessagePayload) {
        return Promise.resolve().then(() => {
            const message = payload?.message;

            clog(levels.INFO, `EVENT ${message?.offset}:`, message?.value?.toString());

            let params: {
                code?: string;
                tags?: TAGS;
                files?: any[];
            };

            try {
                params = checkFormat({
                    // Template code
                    code: allowNullOrEmpty(checkString),
                    // Template tags
                    tags: allowNullOrEmpty(function (value) {
                        const ret = {};

                        for (const tag of Object.keys(value)) {
                            switch (typeof value[tag]) {
                                case "string": ret[tag] = checkString(value[tag]); break;
                                case "number": ret[tag] = checkNumber(value[tag]); break;
                                default:
                                    throwError(errors.INVALID_FORMAT, "Invalid tag", { tags: value });
                            }
                        }
                        return ret;
                    }),
                    // Email file attachment
                    files: allowNullOrEmpty(arrayOf(function (value) {
                        return checkFormat({
                            name: notAllowNullOrEmpty(checkString),
                            path: notAllowNullOrEmpty(checkString)
                        }, value);
                    }))
                }, JSON.parse(message?.value.toString() || "{}"));
            }
            catch (error) {
                // Skip these malfunction message to prevent it stuck in the event queue
                clog(levels.WARNING, `EVENT ${message?.offset} SKIPPED: ${error}`);
                return false;
            }

            // Send email request to the downstream
            return EmailService.Instance.send(params.code, params.tags, params.files).then(() => {
                clog(levels.NORMAL, `EVENT ${message?.offset} PROCESSED: ${params.tags?.TO}`);
                return true;
            }).catch(error => {
                clog(levels.WARNING, `EVENT ${message?.offset} ERROR: ${error}`);

                // When an error occurred, the retry mechanism will be kick in by the Kafka event queue
                // TODO: Some persistence error should be filter out to prevent the event queue stuck
                // TECHNICAL DEBT: No implemented due to time constraint

                throw error;
            });
        });
    }
}

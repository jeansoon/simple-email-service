'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import Sinon from "sinon";
import Kafka, { ConsumerRunConfig } from "kafkajs";
import { CLogger } from 'mxw-libs-clogger';
import { describeError, errors } from 'mxw-libs-errors';
import { TAGS } from './common/template/template-base';
import { EachMessagePayload } from "./common/kafka";
import IngressService from "./ingress-service";
import EmailService, { FileAttachment } from './email/email-service';

let sandbox: Sinon.SinonSandbox;
let fakeConsumer;

const kafkaUrl = "127.0.0.1:9092";
const kafkaGroup = "AWESOME";
const kafkaTopic = "AWESOME-EMAIL";
const kafkaPartition = 0;

describe('Suite: Ingress Service', function () {
    beforeEach(function () {
        sandbox = Sinon.createSandbox();
        sandbox.stub(CLogger.Instance);
    });

    afterEach(function () {
        if (sandbox) {
            sandbox.restore();
            sandbox = undefined;
        }
    });

    it("Initialize and verify ingress channel", function () {
        let fakeProducer: (payload: EachMessagePayload) => Promise<void>;

        const statusCallbacks: {
            [eventName: string]: () => void;
        } = {};

        fakeConsumer = {
            on: function (eventName: string, cb: () => void) {
                expect(eventName).is.a("string");
                expect(cb).is.a("function");
                statusCallbacks[eventName] = cb;
            },
            connect: function () {
                statusCallbacks["consumer.connect"]?.();
                return Promise.resolve();
            },
            subscribe: function (topic: string, fromBeginning: boolean) {
                return Promise.resolve();
            },
            run: function (config: ConsumerRunConfig) {
                expect(config).is.an("object");
                expect(config.eachMessage).is.a("function");

                fakeProducer = config.eachMessage;
                return Promise.resolve();
            },
            disconnect: function () {
                statusCallbacks["consumer.disconnect"]?.();
                return Promise.resolve();
            },
            commitOffsets: function () {
                return Promise.resolve();
            }
        };

        sandbox.stub(Kafka, "Kafka").returns({
            consumer: function () {
                return fakeConsumer;
            }
        });

        return IngressService.init(kafkaUrl, kafkaGroup, kafkaTopic).then(() => {
            expect(IngressService.isEnabled).is.true;
            expect(IngressService.isReadiness).is.true;
        }).then(() => {
            fakeProducer({
                topic: kafkaTopic,
                partition: kafkaPartition,
                message: {
                    key: undefined,
                    value: Buffer.from("{}"),
                    timestamp: "1635912932995",
                    size: 0,
                    attributes: 0,
                    offset: "0"
                }
            });
        });
    });

    it("Process event message", function () {
        const kafkaTopic = "AWESOME-EMAIL";
        const kafkaPartition = 0;
        const TO = "awesome@hello.world";

        sandbox.stub(EmailService, "Instance").get(() => {
            return {
                send: (code?: string, tags?: TAGS, files?: FileAttachment[]) => {
                    expect(code).is.undefined;
                    expect(tags).is.an("object").has.property("TO").is.a("string").eq(TO);
                    expect(files).is.undefined;

                    return Promise.resolve();
                }
            }
        });

        return IngressService.Instance["process"]({
            topic: kafkaTopic,
            partition: kafkaPartition,
            message: {
                key: undefined,
                value: Buffer.from(JSON.stringify({
                    tags: {
                        TO,
                    }
                })),
                timestamp: "1635912932995",
                size: 0,
                attributes: 0,
                offset: "0"
            }
        });
    });

    it("Should skip mulfunction event message", function () {
        const kafkaTopic = "AWESOME-EMAIL";
        const kafkaPartition = 0;
        const TO = "awesome@hello.world";

        return IngressService.Instance["process"]({
            topic: kafkaTopic,
            partition: kafkaPartition,
            message: {
                key: undefined,
                value: Buffer.from(JSON.stringify({
                    tags: {
                        TO,
                        TAG_SHOULD_BE_STRING_OR_NUMBER: {}
                    }
                })),
                timestamp: "1635912932995",
                size: 0,
                attributes: 0,
                offset: "0"
            }
        }).then(processed => {
            expect(processed).is.false;
        });
    });

    it("Should catch send email error", function () {
        const kafkaTopic = "AWESOME-EMAIL";
        const kafkaPartition = 0;
        const TO = "awesome@hello.world";

        sandbox.stub(EmailService, "Instance").get(() => {
            return {
                send: (code?: string, tags?: TAGS, files?: FileAttachment[]) => {
                    errors.throwError("SIMULATE_ERROR", "Simulate send email error", {});
                }
            }
        });

        return IngressService.Instance["process"]({
            topic: kafkaTopic,
            partition: kafkaPartition,
            message: {
                key: undefined,
                value: Buffer.from(JSON.stringify({
                    tags: {
                        TO,
                    }
                })),
                timestamp: "1635912932995",
                size: 0,
                attributes: 0,
                offset: "0"
            }
        }).then(() => {
            errors.throwError("SHOULD_NOT_REACH_HERE", "", {});
        }).catch(error => {
            const { code, message } = describeError(error);

            expect(code).is.a("string").eq("SIMULATE_ERROR");
            expect(message).is.a("string").eq("Simulate send email error");
        });
    });

    it("Cleanup", function () {
        return IngressService.term();
    });

});

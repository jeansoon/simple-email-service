"use strict";

import { clog, levels } from "mxw-libs-clogger";
import { errors, throwError } from "mxw-libs-errors";
import { allowNull, allowNullOrEmpty, arrayOf, checkFormat, checkNumber, checkString, notAllowNullOrEmpty } from "mxw-libs-utils";
import { TAGS } from "./common/template/template-base";
import { CONNECTION_STATUS, StatusCallback, logLevel, KafkaProducer } from "./common/kafka";

export default class ProducerService {
    private static self: ProducerService;

    private producer: KafkaProducer;
    private topic: string;
    private partition: number;

    static get Instance() {
        return this.self || throwError(errors.NOT_INITIALIZED, "ProducerService initialization is required");
    }

    public static get isEnabled() {
        return this.self ? true : false;
    }

    public static get isReadiness() {
        if (this.self && this.self.producer) {
            return CONNECTION_STATUS.READY === this.self.producer.status;
        }
        return false;
    }

    static init(url: string, topic: string, partition: number = 0) {
        return Promise.resolve().then(() => {
            const params: {
                url: string;
                topic: string;
                partition?: number;
            } = checkFormat({
                url: notAllowNullOrEmpty(checkString),
                topic: notAllowNullOrEmpty(checkString),
                partition: allowNull(checkNumber)
            }, {
                url, topic, partition
            });

            const self = new this();

            const statusCallback: StatusCallback = (status) => {
                clog(levels.NORMAL, `Service event producer status is ${status}`);
            };

            clog(levels.NORMAL, `Service event producer status is CONNECTING`);

            self.topic = params.topic;
            self.partition = params.partition;

            const overrides = {
                logLevel: logLevel.ERROR
            };

            return KafkaProducer.init(params.url, overrides, statusCallback).then(producer => {
                self.producer = producer;
                this.self = self;
            });
        });
    }

    static term() {
        return Promise.resolve().then(() => {
            if (this.self) {
                if (this.self.producer) {
                    return this.self.producer.term();
                }
            }
        }).then(() => {
            this.self = undefined;
        });
    }

    public send(to: string, cc?: string, bcc?: string, code?: string, tags?: TAGS, files?: string[]) {
        return Promise.resolve().then(() => {
            const params: {
                to: string;
                cc?: string;
                bcc?: string;
                code?: string;
                tags?: TAGS;
                files?: {
                    name: string;
                    path: string;
                }[];
            } = checkFormat({
                to: notAllowNullOrEmpty(checkString),
                cc: allowNullOrEmpty(checkString),
                bcc: allowNullOrEmpty(checkString),
                // Template code
                code: allowNullOrEmpty(checkString),
                // Template tags
                tags: allowNullOrEmpty(function (value) {
                    const ret = {};

                    for (const tag of Object.keys(value)) {
                        switch (typeof value[tag]) {
                            case "string": ret[tag] = checkString(value[tag]); break;
                            case "number": ret[tag] = checkNumber(value[tag]); break;
                            default:
                                throwError(errors.INVALID_FORMAT, "Invalid tag", { tags: value });
                        }
                    }
                    return ret;
                }),
                files: allowNullOrEmpty(arrayOf(function (value) {
                    return checkFormat({
                        name: notAllowNullOrEmpty(checkString),
                        path: notAllowNullOrEmpty(checkString)
                    }, value);
                }))
            }, {
                to, cc, bcc, code, tags, files
            });

            const message: string = JSON.stringify({
                code: params.code,
                tags: {
                    ...params.tags,
                    TO: params.to,
                    CC: params.cc,
                    BCC: params.bcc
                },
                files: params.files
            });

            return this.producer.send(this.topic, message, this.partition).then(() => {
                clog(levels.NORMAL, `POSTED event: ${message}`);
            });
        });
    }
}

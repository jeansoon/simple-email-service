"use strict";

import nodemailer from "nodemailer";
import { errors, throwError } from "mxw-libs-errors";
import { clog, levels } from "mxw-libs-clogger";
import TemplateService, { Template } from "../common/template/template";
import { TAGS } from "../common/template/template-base";
import { MailOptions } from "nodemailer/lib/sendmail-transport";
import { allowNull, allowNullOrEmpty, arrayOf, checkAny, checkFormat, checkString, notAllowNullOrEmpty } from "mxw-libs-utils";
import Queue from "../common/queue";

export type FileAttachment = {
    name: string;
    path: string;
};

export default class EmailService {
    private static self: EmailService;

    private agent: nodemailer.Transporter;
    private from: string;

    private queue: Queue<void>;

    static get Instance() {
        return this.self || throwError(errors.NOT_INITIALIZED, "EmailService initialization is required");
    }

    public static get isEnabled() {
        return this.self ? true : false;
    }

    static init(url: string, from?: string, concurrent: number = 1, overrides?: any) {
        return Promise.resolve().then(() => {
            if (url) {
                const self = new this();

                self.agent = nodemailer.createTransport(url);

                const info: any = self.agent.options;

                self.from = from ? from : info?.auth?.user;
                self.queue = new Queue(concurrent);

                // Trigger to load template
                return TemplateService.Instance.getTemplate(0, "").then(() => {
                    this.self = self;
                    clog(levels.NORMAL, `Using email provider ${self.from} @ ${info?.host}:${info?.port}`);
                });
            }
        });
    }

    static term() {
        return Promise.resolve().then(() => {
            if (this.self) {
                this.self.agent.close();
            }
        });
    }

    public send(code?: string, tags?: TAGS, files?: FileAttachment[]) {
        return Promise.resolve().then(() => {
            const input: {
                code?: string;
                tags?: TAGS;
                files?: FileAttachment[];
            } = checkFormat({
                code: allowNullOrEmpty(checkString, null),
                tags: allowNull(checkAny, {}),
                files: allowNullOrEmpty(arrayOf(function (value) {
                    return checkFormat({
                        name: notAllowNullOrEmpty(checkString),
                        path: notAllowNullOrEmpty(checkString)
                    }, value);
                }))
            }, { code, tags, files });

            return Promise.resolve().then(() => {
                const language = input.tags["LANGUAGE"] || "en_US";
                const code = input.code ? `${input.code}.${language}` : null;

                clog(levels.NORMAL, `EMAIL CODE: ${code}, TAGS: ${JSON.stringify(input.tags)}, FILES: ${input.files ? "YES" : "N/A"}`);

                return getContent(0, code, input.tags);
            }).then(content => {
                const to = content.tags["TO"] as string || throwError(errors.PARAMETER_MISSING, "Missing TO email address", { tags: content.tags });;
                const cc = content.tags["CC"] as string;
                const bcc = content.tags["BCC"] as string;

                const title = content.title || "";
                const body = content.body || "";

                clog(levels.NORMAL, `EMAIL TO: ${to}${cc ? ", CC: " + cc : ""}${bcc ? ", BCC: " + bcc : ""}, TITLE: ${title}, TAGS: ${JSON.stringify(content.tags)}, FILES: ${input.files ? "YES" : "N/A"}`);

                return this.sendRaw(title, body, to, cc, bcc, input.files).then(() => {
                    clog(levels.NORMAL, `EMAIL SENT TO: ${to}${cc ? ", CC: " + cc : ""}${bcc ? ", BCC: " + bcc : ""}, TITLE: ${title}`);
                });
            });
        });
    }

    private sendRaw(title: string, body: string, to: string, cc?: string, bcc?: string, files?: FileAttachment[]) {
        return Promise.resolve().then(() => {
            const mail: MailOptions = {
                from: this.from,
                to, cc, bcc,
                subject: title,
                html: body
            };

            if (files && 0 < files.length) {
                mail.attachments = files;
            }

            // Control the concurrency request that send to an email provider
            return this.queue.enqueue(() => {
                return this.agent.sendMail(mail);
            });
        });
    }
}

function getContent(orgId: number, code: string, tags?: TAGS) {
    return Promise.resolve().then(() => {
        let title = null;
        let body = null;

        tags = tags || {};

        return Promise.resolve().then(() => {
            if (code) {
                return TemplateService.Instance.getTemplate(orgId, code + ".title");
            }
            if (tags["TITLE"]) {
                return new Template(null, null, null, tags["TITLE"].toString());
            }
        }).then(template => {
            if (template) {
                return template.render(tags).then(result => {
                    title = result.data;
                    tags = { ...tags, ...result.tags };
                });
            }
        }).then(() => {
            if (code) {
                return TemplateService.Instance.getTemplate(orgId, code + ".body");
            }
            if (tags["BODY"]) {
                return new Template(null, null, null, tags["BODY"].toString());
            }
        }).then(template => {
            if (template) {
                return template.render(tags).then(result => {
                    body = result.data;
                    tags = { ...tags, ...result.tags };
                });
            }
        }).then(() => {
            return { title, body, tags };
        });
    });
}

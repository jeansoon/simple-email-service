'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import Sinon from "sinon";
import { CLogger } from 'mxw-libs-clogger';
import { TAGS } from "../common/template/template-base";
import EmailService, { FileAttachment } from "./email-service";

let sandbox: Sinon.SinonSandbox;

describe('Suite: Email Service', function () {
    beforeEach(function () {
        sandbox = Sinon.createSandbox();
        sandbox.stub(CLogger.Instance);
    });

    afterEach(function () {
        if (sandbox) {
            sandbox.restore();
            sandbox = undefined;
        }
    });

    it("Initialize", function () {
        const emailProviderUrl = "smtp://apikey:password@smtp.com:587";
        const emailFrom = "Awesome <no-reply@awesome.com>";
        const concurrency = 3;

        return EmailService.init(emailProviderUrl, emailFrom, concurrency).then(() => {
            expect(EmailService.isEnabled).is.true;
        });
    });

    it("Send email", function () {
        const tags: TAGS = {
            "TO": "alice@awesome.com",
            "TITLE": "Simple Email From Awesome",
            "BODY": "Dear Alice"
        };

        sandbox.stub(EmailService.prototype as any, "sendRaw").callsFake(
            function (title: string, body: string, to: string, cc?: string, bcc?: string, files?: FileAttachment[]) {
                // console.log(arguments);

                expect(title).is.a("string").eq(tags["TITLE"]);
                expect(body).is.a("string").eq(tags["BODY"]);
                expect(to).is.a("string").eq(tags["TO"]);
                expect(cc).is.undefined;
                expect(bcc).is.undefined;
                expect(files).is.undefined;

                return Promise.resolve();
            }
        );

        return EmailService.Instance.send(null, tags);
    });

    it("Send email with template", function () {
        const tags: TAGS = {
            "COMPANY": "Awesome",
            "TO": "alice@awesome.com",
            "CC": "bob@awesome.com,john@awesome.com",
            "BCC": "jeans@awesome.com,richard@awesome.com",
            "NAME": "Alice",
            "TITLE": "Hello From {{COMPANY}}",
            "BODY": "Dear {{NAME}}, How are you?"
        };

        sandbox.stub(EmailService.prototype as any, "sendRaw").callsFake(
            function (title: string, body: string, to: string, cc?: string, bcc?: string, files?: FileAttachment[]) {
                // console.log(arguments);

                expect(title).is.a("string").eq(`Hello From ${tags["COMPANY"]}`);
                expect(body).is.a("string").eq(`Dear ${tags["NAME"]}, How are you?`);
                expect(to).is.a("string").eq(tags["TO"]);
                expect(cc).is.a("string").eq(tags["CC"]);
                expect(bcc).is.a("string").eq(tags["BCC"]);
                expect(files).is.undefined;

                return Promise.resolve();
            }
        );

        return EmailService.Instance.send(null, tags);
    });

    it("Send email with Base64 encoded template", function () {
        const tags: TAGS = {
            "COMPANY": "Awesome",
            "TO": "alice@awesome.com",
            "CC": "bob@awesome.com,john@awesome.com",
            "BCC": "jeans@awesome.com,richard@awesome.com",
            "NAME": "Alice",
            "TITLE": "SGVsbG8gRnJvbSB7e0NPTVBBTll9fQ==",
            "BODY": "RGVhciB7e05BTUV9fSwgSG93IGFyZSB5b3U/"
        };

        sandbox.stub(EmailService.prototype as any, "sendRaw").callsFake(
            function (title: string, body: string, to: string, cc?: string, bcc?: string, files?: FileAttachment[]) {
                // console.log(arguments);

                expect(title).is.a("string").eq(`Hello From ${tags["COMPANY"]}`);
                expect(body).is.a("string").eq(`Dear ${tags["NAME"]}, How are you?`);
                expect(to).is.a("string").eq(tags["TO"]);
                expect(cc).is.a("string").eq(tags["CC"]);
                expect(bcc).is.a("string").eq(tags["BCC"]);
                expect(files).is.undefined;

                return Promise.resolve();
            }
        );

        return EmailService.Instance.send(null, tags);
    });

    it("Cleanup", function () {
        return EmailService.term();
    });

});

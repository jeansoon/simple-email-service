'use strict';

import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import Sinon from "sinon";
import Kafka from "kafkajs";
import { CLogger } from 'mxw-libs-clogger';
import { ProducerRecord } from "./common/kafka";
import ProducerService from "./producer-service";

let sandbox: Sinon.SinonSandbox;
let fakeProducer;

const kafkaUrl = "127.0.0.1:9092";
const kafkaTopic = "AWESOME-EMAIL";
const kafkaPartition = 0;

describe('Suite: Producer Service', function () {
    beforeEach(function () {
        sandbox = Sinon.createSandbox();
        sandbox.stub(CLogger.Instance);
    });

    afterEach(function () {
        if (sandbox) {
            sandbox.restore();
            sandbox = undefined;
        }
    });

    it("Initialize", function () {
        const statusCallbacks: {
            [eventName: string]: () => void;
        } = {};

        fakeProducer = {
            on: function (eventName: string, cb: () => void) {
                expect(eventName).is.a("string");
                expect(cb).is.a("function");
                statusCallbacks[eventName] = cb;
            },
            connect: function () {
                statusCallbacks["producer.connect"]?.();
                return Promise.resolve();
            },
            disconnect: function () {
                statusCallbacks["producer.disconnect"]?.();
                return Promise.resolve();
            },
            send: function (payload: ProducerRecord) { }
        };

        sandbox.stub(Kafka, "Kafka").returns({
            producer: function () {
                return fakeProducer;
            }
        });

        return ProducerService.init(kafkaUrl, kafkaTopic, kafkaPartition).then(() => {
            expect(ProducerService.isEnabled).is.true;
            expect(ProducerService.isReadiness).is.true;
        });
    });

    it("Send event message", function () {
        const to = "alice@awesome.com";
        const cc = "bob@awesome.com,ali@awesome.com";
        const TITLE = "Hello";
        const BODY = "Hello from producer";

        let sent = false;

        sandbox.stub(fakeProducer, "send").callsFake(
            function (payload: ProducerRecord) {
                expect(payload).is.an("object");
                expect(payload).has.property("topic").is.a("string").eq(kafkaTopic);
                expect(payload).has.property("messages").is.an("array").lengthOf(1);
                expect(payload.messages[0]).has.property("partition").is.a("number").eq(kafkaPartition);
                expect(payload.messages[0]).has.property("value").is.a("string").eq(JSON.stringify({
                    tags: {
                        TITLE,
                        BODY,
                        TO: to,
                        CC: cc
                    }
                }));

                sent = true;
                return Promise.resolve();
            }
        );

        return ProducerService.Instance.send(to, cc, null, null, { TITLE, BODY }).then(() => {
            expect(sent).is.true;
        });
    });

    it("Cleanup", function () {
        return ProducerService.term();
    });

});
